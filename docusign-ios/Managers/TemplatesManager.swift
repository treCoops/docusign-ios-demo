//
//  TemplatesManager.swift
//  docusign-ios
//
//  Created by treCoops on 2021-06-11.
//

import Foundation
import DocuSignSDK

class TemplatesManager
{
    static let sharedInstance = TemplatesManager()
    
    var templatesManager: DSMTemplatesManager?
    
    var templateDefinitions: [DSMEnvelopeTemplateDefinition]?
    
    private init() {
        
        if (templatesManager == nil)
        {
            templatesManager = DSMTemplatesManager()
        }
    }
    
    func displayTemplateForSignature(templateId: String, controller: UIViewController, tabData: Dictionary<String, String>, recipientData: Array<DSMRecipientDefault>, customFields:DSMCustomFields?, onlineSign: Bool, attachmentUrl: URL?, completionHandler: @escaping ((UIViewController?, Error?) -> Void))
    {
        var pdfData: Data?
        if (attachmentUrl != nil)
        {
            do {
                pdfData = try Data(contentsOf: attachmentUrl!)
            }
            catch {
                NSLog("Error loading PDF data")
            }
        }
        
        let envelopeDefaults = DSMEnvelopeDefaults()
        envelopeDefaults.recipientDefaults = recipientData.count > 0 ? recipientData : nil
        envelopeDefaults.tabValueDefaults = tabData
        envelopeDefaults.customFields = customFields
        
        templatesManager?.presentSendTemplateControllerWithTemplate (
            withId: templateId,
            envelopeDefaults: nil,
            pdfToInsert: pdfData,
            insertAtPosition: .end,
            signingMode: onlineSign ? .online : .offline,
            presenting: controller,
            animated: true) { (view, error) in
                if let error = error {
                    NSLog("Error encountered during signing: \(error.localizedDescription)")
                }
                if view == nil {
                    NSLog("Warning: Encountered `nil view` during signing.")
                } else {
                    NSLog("DocuSign Native iOS SDK - UI components active")
                }
        }
    }
}
