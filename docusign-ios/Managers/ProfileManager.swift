//
//  ProfileManager.swift
//  docusign-ios
//
//  Created by treCoops on 2021-06-11.
//

import UIKit
import DocuSignSDK


class ProfileManager
{
    static let sharedInstance = ProfileManager()
    
    private init()
    {
        self.clientData = ProfileManager.Static.fakeClientData
    }

    struct Static
    {
        static let demoHostApi: URL? = URL(string: "https://demo.docusign.net/restapi")

        static let integratorKey: String = "1e95886f-7454-4f11-a562-ed25d9b8b4fb"

        static let defaultUsername: String =  "development@xigenix.com"
        static let defaultPassword: String = "xig@Fortress"
        
//        static let tabKey_name: String = "Text dc02e5f6-ff92-4d84-b13d-d82f4c886e81"
//        static let tabKey_address1: String = "Text 05f929aa-b3c6-4c17-bf22-3f7e6bfbe473"
//        static let tabKey_address2: String = "Text 526bf465-583e-426f-9671-04dadbb16dbc"
//        static let tabKey_address3: String = "Text fa6374f6-1fe0-4a71-907a-0c3342791df3"
//        static let tabKey_clientNumber: String = "Text c8985ea9-4126-4c70-832f-b8f06c76fae1"
//        static let tabKey_investmentAmount: String = "Text 9edc0b80-1604-4079-bf0a-1b0b2a5be361"
        
        // fake data used to pre-populate client info
        static let fakeClientData: Dictionary<String, String> = ["firstName":"Tom",
                                                                 "lastName":"Wood",
                                                                 "address":"726 Tennessee St",
                                                                 "city":"San Francisco",
                                                                 "state":"California",
                                                                 "country":"U.S.A.",
                                                                 "zipCode":"94107",
                                                                 "email":"tom.wood@digital.com",
                                                                 "phone":"415-555-1234",
                                                                 "clientNumber":"FA-45231-005",
                                                                 "investmentAmount":"$25,000.00"
        ]
        
        // Momentum demo template
        static let templateIdsMomemtumDemo = ["606f0e95-a419-4280-86ac-234913037962",
                                              "c0f2e494-f418-4793-891a-6087500ae6dc",
                                              "938c63f0-67ad-4561-8e0e-0be3f0f4d73e"]

        static let tabLabelIdFullName = "Text FullName"
        static let tabLabelIdAddressLine1 = "Text Address Line 1"
        static let tabLabelIdAddressLine2 = "Text Address Line 2"
        static let tabLabelIdAddressLine3 = "Text Address Line 3"
        static let tabLabelIdClientNumber = "Text Client Number"
        static let tabLabelIdInvestmentAmount = "Text Investment Amount"

        // Momentum demo template - WireTransfer
        static let templateIdMomemtumDemoWireTransfer = "ff088ecf-5d8b-42c5-9160-569f26beb3a2"
        static let tabLabelIdOwnerFullName = "Text OwnerFullName"
        static let tabLabelIdOwnerEmailAddress = "Text EmailAddress"
        static let tabLabelIdTransferAmount = "Text Amount"
        static let tabLabelIdCurrency = "Text Currency"
        static let tabLabelIdRoutingNumber = "Text RoutingNumber"
        static let tabLabelIdBankName = "Text BankName"
        static let tabLabelIdAccountNumber = "Text AccountNumber"
        static let tabLabelIdAdditionalDetails = "Text AdditionalDetails"
        static let tabLabelIdNameOfWireRecipient = "Text NameOfWireRecipient"
        static let tabLabelIdAddressOfWireRecipient = "Text AddressOfWireRecipient"
        static let tabLabelIdSWIFTCode = "Text SWIFTCode"
        static let tabLabelIdCountry = "Text Country"
        static let tabLabelIdSourceRoutingNumber = "Text SourceRoutingNumber"
        static let tabLabelIdSourceAccountNumber = "Text SourceAccountNumber"
        static let tabLabelIdSenderFullName = "Text SenderFullName"
        
        static let autoDownloadTemplates = true
        static let displayDeveloperNotes = true
    }

    
    // members
    private var mCurrentTemplateId: String = "c5e091d8-0908-4751-9530-fce954f7786b"
    private var mUseOfflineFlow: Bool = true
    private var mAttachmentUrl: URL?
    private var mEnvelopesNeedToSync: Bool = false
    private var clientData = Dictionary<String, String>()
    

    // MARK: Public Methods
    
    func setCurrentTemplateId(templateId: String)
    {
        self.mCurrentTemplateId = templateId
    }
    
    
    func getCurrentTemplateId() -> String
    {
        return self.mCurrentTemplateId
    }
    
    
    func setUseOfflineFlow(useOffline: Bool)
    {
        self.mUseOfflineFlow = useOffline
    }


    func getUseOfflineFlow() -> Bool
    {
        return self.mUseOfflineFlow
    }


    func setAttachmentPath(attachmentUrl: URL)
    {
        self.mAttachmentUrl = attachmentUrl
    }
    
    
    func getAttachmentUrl() -> URL?
    {
        return self.mAttachmentUrl
    }

    
    func setEnvelopesNeedToSync(needToSync: Bool)
    {
        self.mEnvelopesNeedToSync = needToSync
    }
    
    
    func getEnvelopesNeedToSync() -> Bool
    {
        return self.mEnvelopesNeedToSync
    }

    
    func setClientPersonalInfo(firstName:String?, lastName:String?, address:String?, city:String?, state:String?, country:String?, zipCode:String?, email:String?, phone:String?)
    {
        self.clientData["firstName"] = firstName != nil ? firstName : ""
        self.clientData["lastName"] = lastName != nil ? lastName : ""
        self.clientData["address"] = address != nil ? address : ""
        self.clientData["city"] = city != nil ? city : ""
        self.clientData["state"] = state != nil ? state : ""
        self.clientData["country"] = country != nil ? country : ""
        self.clientData["zipCode"] = zipCode != nil ? zipCode : ""
        self.clientData["email"] = email != nil ? email : ""
        self.clientData["phone"] = phone != nil ? phone : ""
    }


    func setClientInvestmentInfo(clientNumber:String, investmentAmount:String)
    {
        self.clientData["clientNumber"] = clientNumber
        self.clientData["investmentAmount"] = investmentAmount
    }

    
    func getClientData() -> Dictionary<String, String>
    {
        return self.clientData
    }
    
    
    func getClientName() -> String
    {
        return self.clientData["firstName"]! + " " + self.clientData["lastName"]!
    }

    
    /**
     Returns Dictionary of the data that will be passed into the template
     */
    func getTemplateTabData(templateId:String) -> Dictionary<String,String>
    {
        var tabData = Dictionary<String, String>()
        
        tabData[ProfileManager.Static.tabLabelIdFullName] = self.getClientName()
        tabData[ProfileManager.Static.tabLabelIdAddressLine1] = self.clientData["address"]
        tabData[ProfileManager.Static.tabLabelIdAddressLine2] = self.clientData["city"]! + ", " + self.clientData["state"]!
        tabData[ProfileManager.Static.tabLabelIdAddressLine3] = self.clientData["country"]! + " " + self.clientData["zipCode"]!
        tabData[ProfileManager.Static.tabLabelIdClientNumber] = self.clientData["clientNumber"]
        tabData[ProfileManager.Static.tabLabelIdInvestmentAmount] = self.clientData["investmentAmount"]
    
        return tabData
    }

    func getTemplateRecipientData(templateId:String) -> Array<DSMRecipientDefault>
    {
        let recipientDatum = DSMRecipientDefault()
        // Use recipient roleName (other option to use recipient-id) to find unique recipient in the template
        recipientDatum.recipientRoleName = "claimant-roleName"
        recipientDatum.recipientSelectorType = .recipientRoleName
        recipientDatum.recipientType = .inPersonSigner
        // In-person-signer name
        recipientDatum.inPersonSignerName = "Tom Wood"
        // Host name (must match the name on the account) and email
        recipientDatum.recipientName = "docusignsdk user"
        recipientDatum.recipientEmail = "docusignsdk.user@dsxtr.com"
        return [recipientDatum]
    }
    
    /**
     Returns CustomFields that will be passed into the template
     */
    func getCustomFieldsData(templateId:String) -> DSMCustomFields? {
    
        
        print("asdsa");
        
        let textCustomField = DSMTextCustomField()
        textCustomField.name = "Investor"
        textCustomField.value = "Tom Wood"
        textCustomField.show = true

        let existingTextCustomField = DSMTextCustomField()
        existingTextCustomField.name = "templateUsageRestriction"
        existingTextCustomField.value = "Code-A-123-444-California"
        existingTextCustomField.show = true
        
        let customFields = DSMCustomFields()
        customFields.textCustomFields = [textCustomField, existingTextCustomField]
        return customFields
    }
}
