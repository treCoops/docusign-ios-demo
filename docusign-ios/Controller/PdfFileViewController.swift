//
//  PdfFileViewController.swift
//  docusign-ios
//
//  Created by treCoops on 2021-06-11.
//

import UIKit
import WebKit
import DocuSignSDK

class PdfFileViewController: UIViewController {
    
    private var mAttachmentUrl: URL?
    let fakeClientData: Dictionary<String, String> = ["firstName":"Tom",
                                                             "lastName":"Wood",
                                                             "address":"726 Tennessee St",
                                                             "city":"San Francisco",
                                                             "state":"California",
                                                             "country":"U.S.A.",
                                                             "zipCode":"94107",
                                                             "email":"tom.wood@digital.com",
                                                             "phone":"415-555-1234",
                                                             "clientNumber":"FA-45231-005",
                                                             "investmentAmount":"$25,000.00"
    ]

    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let pdfFileName = "TGK-Capital-Template"
        if let documentURL = Bundle.main.url(forResource: pdfFileName, withExtension: "pdf", subdirectory: nil, localization: nil)
        {
            do {
                let data = try Data(contentsOf: documentURL)
                self.webView.load(data, mimeType: "application/pdf", characterEncodingName: "", baseURL: documentURL.deletingLastPathComponent())
                self.webView.isHidden = false
                
                mAttachmentUrl = documentURL
                
                self.promptDevAttachment()
            }
            catch {
                NSLog("Error loading PDF file as attachment.")
            }
        }

    }
    
    private func promptDevAttachment()
    {
        let title = String.developerNotesTitle(with: true)
        let message = "For sample purposes, we have included a PDF document that has been attached. Ordinarily, you would guide the user through selecting an external PDF document to attach to the envelope."
        let attachmentAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        attachmentAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
            self.callBackend(url: self.mAttachmentUrl!)
        }))

        self.present(attachmentAlert, animated: true, completion: nil)
    }
    
    private func callBackend(url: URL){
        let templateId: String = ProfileManager.sharedInstance.getCurrentTemplateId()
        let tabData: Dictionary = ProfileManager.sharedInstance.getTemplateTabData(templateId:templateId)
        let recipientData: Array = ProfileManager.sharedInstance.getTemplateRecipientData(templateId:templateId)
//        let onlineSign: Bool = !ProfileManager.sharedInstance.getUseOfflineFlow()
        let attachmentUrl: URL? = ProfileManager.sharedInstance.getAttachmentUrl()
        let customFields = ProfileManager.sharedInstance.getCustomFieldsData(templateId:templateId)

        TemplatesManager.sharedInstance.displayTemplateForSignature(templateId: templateId, controller: self, tabData: tabData, recipientData: recipientData, customFields:customFields, onlineSign: false, attachmentUrl: attachmentUrl) { (controller, errMsg) in
            
            let vcIndex = self.navigationController?.viewControllers.firstIndex(where: { (viewController) -> Bool in
                if let _ = viewController as? PdfFileViewController {
                    return true
                }
                return false
            })
            
            let clientVC = self.navigationController?.viewControllers[vcIndex!] as! PdfFileViewController
            self.navigationController?.popToViewController(clientVC, animated: true)
        }
    }
    
}
