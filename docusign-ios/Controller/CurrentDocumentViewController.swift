//
//  ViewController.swift
//  docusign-ios
//
//  Created by treCoops on 2021-06-10.
//

import UIKit
import DocuSignSDK

class CurrentDocumentViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let nib = UINib(nibName: "DocumentTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "cell_file")
        
    }


}

extension CurrentDocumentViewController :  UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DocumentTableViewCell =  (self.tableView.dequeueReusableCell(withIdentifier: "cell_file") as! DocumentTableViewCell?)!

        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let apptCell: DocumentTableViewCell = cell as! DocumentTableViewCell
        
        switch indexPath.row
        {
            case 0:
                apptCell.txtFileName.text = "Sample PDF"
                break
                
            default:
                break
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if (indexPath.row == 0)
        {
            self.performSegue(withIdentifier: "segueDocument", sender: self)
        }
    }
    
    
}

