//
//  HomeViewController.swift
//  docusign-ios
//
//  Created by treCoops on 2021-06-11.
//

import UIKit
import DocuSignSDK

class HomeViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btnCreteNewTapped(_ sender: UIButton) {
        
        self.promptDevAction(composeEnvelopeHandler: presentComposeEnvelope)
        
    }
    
    @IBAction func btnSignCurrent(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "segueViewCurrentDocument", sender: nil)
        
    }
    
    private func promptDevAction(composeEnvelopeHandler handler: @escaping (_ signingMode: DSMSigningMode) -> Void)
    {
        let title = String.developerNotesTitle(with: true)
        let message = "You can either compose an envelope in online or offline mode. You would need to check for network connectivity and present the appropriate view controller."
        let agreementAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        // add Ok action
        agreementAlert.addAction(UIAlertAction(title: "Online Envelope", style: .default, handler: { (action) in
            handler(.online)
        }))
        
        agreementAlert.addAction(UIAlertAction(title: "Offline Envelope", style: .default, handler: { (action) in
            handler(.offline)
        }))
        agreementAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))
        self.present(agreementAlert, animated: true, completion: nil)
    }

    private func presentComposeEnvelope(_ signingMode: DSMSigningMode) -> Void
    {
        if #available(iOS 11.0, *) {
            EnvelopesManager.sharedInstance.presentComposeEnvelopeViewController(self, signingMode)
        } else {
            let alertController = UIAlertController(title: "iCloud Entitlement required", message: "For iOS 10 and below, iCloud entitlements must be added and DSM_SETUP_ICLOUD_DOCUMENT_ENABLED set to true for document picker usage.", preferredStyle: .actionSheet)
            
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                EnvelopesManager.sharedInstance.presentComposeEnvelopeViewController(self, signingMode)
            }
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
}



