//
//  NSString+Extensions.swift
//  docusign-ios
//
//  Created by treCoops on 2021-06-11.
//

import Foundation
import DocuSignSDK

extension String {
   static func developerNotesTitle(with sdkVersion:Bool) -> String {
        if sdkVersion {
            return "Developer's Notes (\(DSMManager.buildVersion())"
        }
        return "Developer's Notes"
  }
}

