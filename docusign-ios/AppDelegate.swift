//
//  AppDelegate.swift
//  docusign-ios
//
//  Created by treCoops on 2021-06-10.
//

import UIKit
import DocuSignSDK

@main
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        var configurations = DSMManager.defaultConfigurations()
        configurations[DSM_SETUP_OFFLINE_SIGNING_HIDE_ALERTS_KEY] = DSM_SETUP_TRUE_VALUE
        
        if #available(iOS 11.0, *) {
            configurations[DSM_SETUP_ICLOUD_DOCUMENT_ENABLED] = DSM_SETUP_TRUE_VALUE
        } else {
            configurations[DSM_SETUP_ICLOUD_DOCUMENT_ENABLED] = DSM_SETUP_FALSE_VALUE
        }
        
        DSMManager.setup(withConfiguration: configurations)
    
        DSMManager.login(withEmail: "development@xigenix.com",
                         password: "xig@Fortress",
                         integratorKey: "1e95886f-7454-4f11-a562-ed25d9b8b4fb",
                         host: URL(string: "https://demo.docusign.net/restapi")!) { (accountInfo, error) in

            if (error != nil)
            {
                print("Error logging in: \(String(describing: error))")
            }
            else
            {
                print("User authenticated")
            }
        }
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

