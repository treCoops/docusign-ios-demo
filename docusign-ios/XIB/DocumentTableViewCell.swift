//
//  DocumentTableViewCell.swift
//  docusign-ios
//
//  Created by treCoops on 2021-06-11.
//

import UIKit

class DocumentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var txtFileName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
